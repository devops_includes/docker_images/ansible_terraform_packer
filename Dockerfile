FROM python:3.11.4

LABEL maintainer="Avengerist"
LABEL version=1.0.0

ENV ANSIBLE_VERSION=8.2.0
ENV ANSIBLE_LINT_VERSION=6.17.2

ENV MOLECULE_VERSION=5.1.0
ENV MOLECULE_PLUGINS_VERSION=23.4.1

ENV TERRAFORM_VERSION=1.5.4

ENV PACKER_VERSION=1.9.2

ENV DOCKER_VERSION=24.0.5

RUN apt-get update && apt-get install -y --no-install-recommends \
    wget \
    unzip \
    git \
    && pip install \
    ansible==${ANSIBLE_VERSION} \
    ansible_lint==${ANSIBLE_LINT_VERSION} \
    molecule==${MOLECULE_VERSION} \
    molecule-plugins[docker]==${MOLECULE_PLUGINS_VERSION} \
    && rm -rf /var/lib/apt/lists/* \ 
    && apt-get clean


ADD https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_linux_amd64.zip .
ADD https://releases.hashicorp.com/packer/${PACKER_VERSION}/packer_${PACKER_VERSION}_linux_amd64.zip .
ADD https://download.docker.com/linux/static/stable/x86_64/docker-${DOCKER_VERSION}.tgz .

RUN unzip terraform_${TERRAFORM_VERSION}_linux_amd64.zip -d /bin && rm -f terraform_${TERRAFORM_VERSION}_linux_amd64.zip
RUN unzip packer_${PACKER_VERSION}_linux_amd64.zip -d /bin && rm -f packer_${PACKER_VERSION}_linux_amd64.zip
RUN tar xfvz docker-${DOCKER_VERSION}.tgz --strip 1 -C /bin docker/docker && rm -f docker-${DOCKER_VERSION}.tgz

ENTRYPOINT ["/bin/bash", "-c"] 
